import React, { Component } from 'react';
import './styles.scss';

class App extends Component {
  render() {
    return (
      <div className="projectViewer">
        {/* CODE HERE */}
        {this.props.structure}
      </div>
    );
  }
}

export default App;
